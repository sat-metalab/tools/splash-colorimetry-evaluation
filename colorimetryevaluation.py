import numpy as np
import enum
import datetime

# Plots and image reading

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

# Tools for images comparisons

from skimage.metrics import structural_similarity as ssim
from skimage.metrics import mean_squared_error as mse
from skimage.metrics import peak_signal_noise_ratio as psnr
from skimage import exposure

from sporco.metric import gmsd

from scipy import fftpack, ndimage, signal

# To save interractive plots 
import pickle

class Spectra(enum.Enum):
   Power = 1
   Magnitude = 2
   Phase = 3


class DisplayMode(enum.Enum):
    SingleRGB = 1
    SingleGreyScale = 2
    ChannelBychannel = 3


# Load and plot images
def loadImages(input_file: str = 'data/input.png', output_file: str = 'data/output.png', plot_images: bool = True):
    
    '''
    Loads and plots images if images are of same shape 
    args:
        - input_file: path to input image
        - output_file: path to output image
        - plot_image: plot if true
    returns:
        - input_image: numpy.ndarray
        - output_image: numpy.ndarray
    '''
    
    input_image = mpimg.imread(input_file)
    output_image = mpimg.imread(output_file)
    
    if not input_image.shape == output_image.shape:
        raise RuntimeError('Images must be same size')
    
    if plot_images:
        
        fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 4),
                         sharex=True, sharey=True)

        axes[0].imshow(input_image)
        axes[0].set_title('Input')
        axes[1].imshow(output_image)
        axes[1].set_title('Ouput')

        plt.tight_layout()
        
        plt.show()
    
    return (input_image,output_image)


# Multichannel GMSD index and map
def GMSD(input_image, output_image, plot_fig: bool = True, save_fig: bool = False, 
         interactive: bool = False, path: str = '', file_name: str = '',
         display_mode: DisplayMode = DisplayMode.SingleGreyScale):

    '''
    Computes and plots GMSD index and map channel by channel
    args:
        - input_image: numpy.ndarray
        - output_image: numpy.ndarray
        - plot_fig: plot if true
        - save_fig: save the plot if true
        - interactive: save binary plot if true
        - path: where to save figure
        - file_name: name of the figure to be saved
        - display_mode: mode to display the gmds map
    returns:
        - gmsd_index: float
        - gmsd_map: numpy.ndarray
    '''

    # gmsd_index, gmsd_map
    s0, m0 = gmsd(input_image[:,:,0], output_image[:,:,0],returnMap=True)
    s1, m1 = gmsd(input_image[:,:,1], output_image[:,:,1],returnMap=True)
    s2, m2 = gmsd(input_image[:,:,2], output_image[:,:,2],returnMap=True)

    gmsd_index = 1/3 * (s0 + s1 + s2)

    n,m = m0.shape
    gmsd_map = np.ndarray(shape=(n,m,3))
    for i in range(n):
        for j in range(m):
            gmsd_map[i][j][0] = m0[i][j]
            gmsd_map[i][j][1] = m1[i][j]
            gmsd_map[i][j][2] = m2[i][j]

    if save_fig or plot_fig:

        if display_mode == DisplayMode.ChannelBychannel:

            fig, axs = plt.subplots(1, 3, figsize=(15, 5))
            fig.suptitle(f'GMSD: {gmsd_index:.4f}')

            im = axs[0].imshow(m0)
            axs[0].set_title(f'Channel 1 GMSD: {s0:.4f}')
            fig.colorbar(im, ax=axs[0], fraction=0.046, pad=0.04)
            im = axs[1].imshow(m1)
            axs[1].set_title(f'Channel 2 GMSD: {s1:.4f}')
            fig.colorbar(im, ax=axs[1], fraction=0.046, pad=0.04)
            im = axs[2].imshow(m2)
            axs[2].set_title(f'Channel 3 GMSD: {s2:.4f}')
            fig.colorbar(im, ax=axs[2], fraction=0.046, pad=0.04)

            title = 'channel_by_channel_gmsd_map'

        elif display_mode == DisplayMode.SingleRGB:

            fig = plt.figure(figsize = (5,5))
            plt.imshow(gmsd_map)
            plt.title((f'GMSD: {gmsd_index:.4f}'))
            title = 'single_rgb_gmsd_map'

        elif display_mode == DisplayMode.SingleGreyScale:

            fig = plt.figure(figsize = (5,5))
            plt.imshow(np.mean(gmsd_map,2))
            plt.colorbar(fraction=0.046, pad=0.04)
            plt.title((f'GMSD: {gmsd_index:.4f}'))
            title = 'single_grey_scale_gmsd_map'

        else:
            raise RuntimeError('Invalid display mode')

        if save_fig:
            
            if file_name == '':
                # to be sure figures are not overwriten
                date = datetime.datetime.now()
                file_name += str(date)[0:-7] + ' ' + title + '.png'

            plt.savefig(path + file_name)
            if interactive:
                pickle.dump(fig, open(path + 'Interactive/' + file_name + '.fig.pickle', 'wb'))
        
        if plot_fig:
            plt.show()

        plt.clf()

    return gmsd_index, gmsd_map


# Multichannel SSIM index and map
def SSIM(input_image, output_image, plot_fig: bool = True, save_fig: bool = False, 
         interactive: bool = False, path: str = '', file_name: str = '',
         display_mode: DisplayMode = DisplayMode.SingleGreyScale):
    
    '''
    Multichannel SSIM, computes and plots SSIM index and map
    args:
        - input_image: numpy.ndarray
        - output_image: numpy.ndarray
        - plot_fig: plot if true
        - save_fig: save the plot if true
        - interactive: save binary plot if true
        - path: where to save figure
        - file_name: name of the figure to be saved
        - display_mode: mode to display the ssim map
    returns:
        - ssim_index: float
        - ssim_map: numpy.ndarray
    '''

    ssim_index, ssim_map = ssim(input_image, output_image, multichannel=True, full = True,
                                data_range=output_image.max() - output_image.min())
    
    
    if plot_fig or save_fig:

        if display_mode == DisplayMode.ChannelBychannel:

            fig, axs = plt.subplots(1, 3, figsize=(15, 5))
            fig.suptitle(f'SSIM: {ssim_index:.4f}')

            m0 = ssim_map[:,:,0]
            s0 = np.mean(m0)
            m1 = ssim_map[:,:,1]
            s1 = np.mean(m1)
            m2 = ssim_map[:,:,2]
            s2 = np.mean(m2)

            im = axs[0].imshow(m0)
            axs[0].set_title(f'Channel 1 SSIM: {s0:.4f}')
            fig.colorbar(im, ax=axs[0], fraction=0.046, pad=0.04)
            im = axs[1].imshow(m1)
            axs[1].set_title(f'Channel 2 SSIM: {s1:.4f}')
            fig.colorbar(im, ax=axs[1], fraction=0.046, pad=0.04)
            im = axs[2].imshow(m2)
            axs[2].set_title(f'Channel 3 SSIM: {s2:.4f}')
            fig.colorbar(im, ax=axs[2], fraction=0.046, pad=0.04)

            title = 'channel_by_channel_ssim_map'

        elif display_mode == DisplayMode.SingleRGB:

            fig = plt.figure(figsize = (5,5))
            plt.imshow(ssim_map)
            plt.title(f'SSIM: {ssim_index:.4f}')
            title = 'single_rgb_ssim_map'

        elif display_mode == DisplayMode.SingleGreyScale:

            fig = plt.figure(figsize = (5,5))
            plt.imshow(np.mean(ssim_map,2))
            plt.colorbar(fraction=0.046, pad=0.04)
            plt.title(f'SSIM: {ssim_index:.4f}')
            title = 'single_grey_scale_ssim_map'

        else:
            raise RuntimeError('Invalid display mode')


        if save_fig:
            if file_name == '':
                # to be sure figures are not overwriten
                date = datetime.datetime.now()
                file_name += str(date)[0:-7] + ' ' + title + '.png'

            plt.savefig(path + file_name)
            if interactive:
                pickle.dump(fig, open(path + 'Interactive/' + file_name + '.fig.pickle', 'wb'))

        if plot_fig:    
            plt.show()
    
    plt.clf()
            
    return ssim_index, ssim_map


# Power, Magnitude and Phase spectrum
def inputOutputSpectra(input_image, output_image, spectra: Spectra = Spectra.Power,
                       path: str = '', file_name: str = '', interactive: bool = False,
                       plot_fig: bool = True, save_fig: bool = False, multichannel: bool = True):
    
    '''
    PLots the fourier transform of multichannel images
    args:
        - input_image: numpy.ndarray
        - output_image: numpy.ndarray
        - plot_fig: plot if true
        - save_fig: save the plot if true
        - interactive: save binary plot if true
        - path: where to save figure
        - file_name: name of the figure to be saved
        - spectra: type of spectra (power, magnitude, phase)
        - multichannel
    returns:
        - input_spectrum: np.ndarray
        - output_spectrum: np.ndarray
    '''

    input_f = np.fft.fft2(input_image, norm='ortho')
    input_fshift = np.fft.fftshift(input_f)
    
    output_f = np.fft.fft2(output_image, norm='ortho')
    output_fshift = np.fft.fftshift(output_f)
    
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 5),
                             sharex=True, sharey=True)
      
    if spectra == Spectra.Power:
        
        input_spectrum = (np.abs(input_fshift).astype(np.uint8))**2
        output_spectrum = (np.abs(output_fshift).astype(np.uint8))**2
        
        fig.suptitle('Power Spectrum')
        title = 'power_spectrum'
        
    elif spectra == Spectra.Magnitude:
        
        input_spectrum = np.abs(input_fshift).astype(np.uint8)
        output_spectrum = np.abs(output_fshift).astype(np.uint8)
        
        fig.suptitle('Magnitude Spectrum')
        title = 'magnitude_spectrum'
        
    elif spectra == Spectra.Phase:
        
        input_spectrum = np.angle(input_fshift).astype(np.uint8)
        output_spectrum = np.angle(output_fshift).astype(np.uint8)
        
        fig.suptitle('Phase Spectrum')
        title = 'phase_spectrum'
        
    else: 

        raise RuntimeError('Invalid spectra type')


    if plot_fig or save_fig:

        if multichannel:

            im = axes[0].imshow(input_spectrum)
            axes[0].set_title('Input')
            #fig.colorbar(im, ax=axes[0], fraction=0.046, pad=0.04)
            
            im = axes[1].imshow(output_spectrum)
            axes[1].set_title('Ouput')
            #fig.colorbar(im, ax=axes[1], fraction=0.046, pad=0.04)
        
        else:

            im = axes[0].imshow(input_spectrum,cmap='gray')
            axes[0].set_title('Input')
            fig.colorbar(im, ax=axes[0], fraction=0.046, pad=0.04)
            
            im = axes[1].imshow(output_spectrum,cmap='gray')
            axes[1].set_title('Ouput')
            fig.colorbar(im, ax=axes[1], fraction=0.046, pad=0.04)

        plt.tight_layout()
        
        if save_fig:
            if file_name == '':
                # to be sure figures are not overwriten
                date = datetime.datetime.now()
                file_name += str(date)[0:-7] + ' ' + title + '.png'

            plt.savefig(path + file_name)
            if interactive:
                pickle.dump(fig, open(path + 'Interactive/' + file_name + '.fig.pickle', 'wb'))

        if plot_fig:
            plt.show()

    plt.clf()

    return input_spectrum, output_spectrum


def differenceSpectrum(input_image, output_image, spectra: Spectra = Spectra.Power,
                       path: str = '', file_name: str = '',
                       plot_fig: bool = True, save_fig: bool = False, interactive: bool = False,
                       multichannel: bool = True):

    '''
    Plots the fourier transform of the difference of multichannel images
    args:
        - input_image: numpy.ndarray
        - output_image: numpy.ndarray
        - plot_fig: plot if true
        - save_fig: save the plot if true
        - interactive: save binary plot if true
        - path: where to save figure
        - file_name: name of the figure to be saved
        - spectra: type of spectra (power, magnitude, phase)
        - multichannel
    returns:
        - spectrum: np.ndarray spectrum of the difference
    '''

    diff = input_image - output_image 
    if np.amin(diff) < 0:
        diff += np.amin(diff)
    # diff = diff * (1 / (np.amax(diff) - np.amin(diff)))
    
    f = np.fft.fft2(diff, norm='ortho')
    fshift = np.fft.fftshift(f)
    
    if spectra == Spectra.Power:
        spectrum = (np.abs(fshift).astype(np.uint8))**2
        title = 'Power Spectrum'
        
    elif spectra == Spectra.Magnitude:
        spectrum = np.abs(fshift).astype(np.uint8)
        title = 'Magnitude Spectrum'
        
    elif spectra == Spectra.Phase:
        spectrum = np.angle(fshift).astype(np.uint8)
        title = 'Phase Spectrum'
        
    else: 
        raise RuntimeError('Invalid spectra type')
        
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 5),
                             sharex=True, sharey=True)

    if plot_fig or save_fig:

        if multichannel:
            axes[0].imshow(diff)
            axes[0].set_title('Image difference')
            axes[1].imshow(spectrum)
            axes[1].set_title(title)
        else:
            im = axes[0].imshow(diff,cmap='gray')
            axes[0].set_title('Image difference')
            im = axes[1].imshow(spectrum,cmap='gray')
            axes[1].set_title(title)
            fig.colorbar(im, ax=axes[1], fraction=0.046, pad=0.04)
        
        if save_fig:
            if file_name == '':
                # to be sure figures are not overwriten
                date = datetime.datetime.now()
                file_name += str(date)[0:-7] + '_' + title + '.png'

            plt.savefig(path + file_name)
            if interactive:
                pickle.dump(fig, open(path + 'Interactive/' + file_name + '.fig.pickle', 'wb'))

        plt.tight_layout()

        if plot_fig:
            plt.show()

    plt.clf()

    return spectrum


def histograms(input_image, output_image, path: str = '', file_name: str = '',
               plot_fig: bool = True, save_fig: bool = False):

    '''
    compute and plot exposure histograms for each channels
    args:
        - input_image: numpy.ndarray
        - output_image: numpy.ndarray
        - plot_fig: plot if true
        - save_fig: save the plot if true
        - path: where to save figure
        - file_name: name of the figure to be saved
    '''

    fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(8, 8))

    for i, img in enumerate((input_image, output_image)):
        for c, c_color in enumerate(('red', 'green', 'blue')):
            img_hist, bins = exposure.histogram(img[..., c], source_range='dtype')
            axes[c, i].plot(bins, img_hist / img_hist.max())
            img_cdf, bins = exposure.cumulative_distribution(img[..., c])
            axes[c, i].plot(bins, img_cdf)
            axes[c, 0].set_ylabel(c_color)
            axes[c, i].set_xlim(left=0)

    axes[0, 0].set_title('Input')
    axes[0, 1].set_title('Output')

    title = 'Exposure histograms'
    fig.suptitle(title)

    plt.tight_layout()

    if save_fig:
        if file_name == '':
            # to be sure figures are not overwriten
            date = datetime.datetime.now()
            file_name += str(date)[0:-7] + ' ' + title
        plt.savefig(path + file_name + '.png')

    if plot_fig:
        plt.show()

    plt.clf()


# Functions specific to splash script
def saveInputOutput(input_image: np.ndarray, output_image: np.ndarray, path: str, date: str, no: str, 
                    plot_fig: bool = False,  interactive: bool = False):

    '''
    args:
        - input_image
        - output_image
        - path: path of the folder where data are saved
        - date:
        - no: plot number
        - plot_fig: plot the figure in if set to true
        - interactive: save binary plot if true
    raises:
        - Runtime Error if images are not of same size
    '''

    if not input_image.shape == output_image.shape:
        raise RuntimeError('Images must be same size')

    # Plots both input and output on the same figure 
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 4),
                             sharex=True, sharey=True)
    axes[0].imshow(input_image)
    axes[0].set_title('Input')
    axes[1].imshow(output_image)
    axes[1].set_title('Ouput')
    axes[0].get_yaxis().set_visible(False)
    axes[0].get_xaxis().set_visible(False)
    axes[1].get_yaxis().set_visible(False)
    axes[1].get_xaxis().set_visible(False)
    plt.tight_layout()

    file_name =  path + date + ' input_output_' + no + '.png'
    plt.savefig(file_name)
    if interactive:
        pickle.dump(fig, open(path + 'Interactive/' + date + 'input_output_' + no + '.fig.pickle', 'wb'))

    if plot_fig:
        plt.show()

    plt.clf()

    # Save the output just in case you want to run further evaluation
    output_name = path + date + 'output' + str(no) + '.png'
    input_name = path + date + 'input' + str(no) + '.png'
    mpimg.imsave(output_name,output_image)
    mpimg.imsave(input_name,input_image)

    return


def lin2s(x: np.array):
    '''
    Linear rgb to srgb conversion
    arg:
        - x: input matrix in linear rgb format
    returns:
        - srgb conversion
    '''
    a = 0.055
    return np.where(x <= 0.0031308,
                 x * 12.92,
                 (1 + a) * pow(x, 1 / 2.4) - a)