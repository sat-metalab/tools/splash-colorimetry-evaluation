import splash

import time
import datetime # For files' name
import csv # For csv writing
import cv2

import sys
sys.path.append('../') # module location
import os
import getopt # For script args parsing
import warnings
 
# To avoid error when called outside of main thread
import matplotlib
matplotlib.use('Agg')

# For output grabber
import enum
class Output(enum.Enum):
   Camera = 1
   Window = 2

class Mode(enum.Enum):
    Regular = 1
    ImmersiveSpace = 2

from colorimetryevaluation import *
from grabber import Grabber

# For files' name
# folder/yyyy-mm-dd hh:mm:ss figname_no.png
now = str(datetime.datetime.now())
date = now[0:10] + '_' + now[11:-7]
no = 0 # Figure number
folder = ''
directory = '' # wd

update = False # set to true when a new input is loaded
lock = False

def onInputChanged(object, attribute):

    '''
    Callback on input image file changed
    Simply sets update to True to trigger evaluation tools computation in the main loop of the script
    '''

    # TODO
    # Check if input is of compatible type with the tool

    if not lock: 
        global update
        update = True

        print(str(datetime.datetime.now())[0:-7] + " : Input changed")

def initializeConfig(camera_path,camera_parameters,icc_profile,manual_exposure,exposure):

    '''
    Set the right configuration for the tool to work right away with the default splash configuration
    '''

    if output == Output.Window:
        # Upload right mesh and input image
        splash.set_object_attribute("image","file",directory + "/color_map.png")
        splash.set_object_attribute("mesh","file",directory + "/square_plane.obj")

        # Add sink to grab output
        global sink
        sink = splash.Sink()
        if sixteenbpc:
            print(str(datetime.datetime.now())[0:-7] + " : Set sink render in 16 bit per components")
            sink.set_sixteenBpc(True)
            time.sleep(2)
        sink.link_to("window_1_cam1_warp")
        sink.open()
        # Place camera for distorsion-free output 
        # Only works for a 1 by 1 square plane for now
        splash.set_objects_of_type("camera","eye",[ 0, 0, 1 ])
        splash.set_objects_of_type("camera","target",[ 0, 0, 0 ])
        splash.set_objects_of_type("camera","up",[ 0, 1, 0 ])
        splash.set_objects_of_type("camera","fov",90)
        # Save config
        splash.set_world_attribute("save",directory + "/config_colo.json")
        # Update
        global update
        update = True

    if output == Output.Camera:
        # Upload markers for projection detection
        splash.set_object_attribute("image","file",directory + '/' + markers)
        # Camera grabber parameters
        time.sleep(5)
        global grabber
        grabber = Grabber(camera_path)
        if undistort:
            grabber.loadUndistortionParameters(camera_parameters)
        if use_icc:
            grabber.loadIccProfile(icc_profile)
        if manual_exposure:
            grabber.exposure = exposure
        grabber.open()
        if mode == Mode.Regular:
            splash.set_objects_of_type("camera", "flashBG", '')
            grabber.findCorners()
            grabber.controlMarkerDetection(folder)
    
    print(str(datetime.datetime.now())[0:-7] + " : Config Initialized")


def grab(input_image, width: int,height: int):

    # Grab output from sink
    if output == Output.Window:
        # Resize output
        size = [width,height]
        splash.set_object_attribute('window_1','size', size)
        splash.set_object_attribute('cam1','size', size)
        sink.set_size(width,height)

        time.sleep(2) # We wait to update the output to be sure it has been loaded
        frame = sink.grab()

        if sixteenbpc: # 16bpc
            print("16 bpc")
            n = len(frame)
            print(n)
            output_image = []
            # very unefficient way but don't know how to do otherwise
            for i in range(n//2):
                item = int.from_bytes(frame[2*i:2*i+2],byteorder='little',signed = False)
                output_image.append(item)
            output_image = np.reshape(output_image,(height,width,4))
            output_image = output_image.astype(np.float32) /(2**16 -1) # map on 0-1
        else: # 8bpc
            output_image = list(frame)
            output_image = np.array(output_image)
            output_image = np.reshape(output_image,(height,width,4),order='C')
            output_image = output_image.astype(np.float32) / 255 # map on 0-1
        
        output_image = output_image[:,:,:3] # last channel filled with ones
        output_image = lin2s(output_image)

    # Or grab output from camera capture
    if output == Output.Camera:
        # We wait to update the output to be sure it has been loaded
        grabber.wait()
        grabber.grab()
        grabber.retrieve()
        output_image = grabber.projection
        if use_icc:
            output_image = grabber.applyTransform(output_image)
        elif merge_mertens:
            fusion  = grabber.mergeMertens()
            output_image = np.where(fusion > 0, fusion, 0)
        input_image = cv2.resize(input_image,(output_image.shape[1],output_image.shape[0]))

    return input_image, output_image


def computemetrics(input_image, output_image, date, folder:str = "", no: str = '0', compute: bool = True):

    # Tools to evaluation the quality of the output

    # GMSD & SSIM
    gmsd_index, gmsd_map = GMSD(input_image, output_image, plot_fig=False, save_fig=True, display_mode=DisplayMode.SingleGreyScale,
                                path=folder, file_name= date + '_gmsd_' + no + '.png', interactive=interactive)

    GMSD(input_image, output_image, plot_fig=False, save_fig=True, display_mode=DisplayMode.ChannelBychannel,path=folder, 
         file_name= date + '_gmsd_channel_by_channel_' + no + '.png', interactive=interactive)

    ssim_index, ssim_map = SSIM(input_image, output_image, plot_fig=False, save_fig=True, display_mode=DisplayMode.SingleGreyScale,
                                path=folder, file_name= date + '_ssim_' + no + '.png', interactive=interactive)

    SSIM(input_image, output_image, plot_fig=False, save_fig=True, display_mode=DisplayMode.ChannelBychannel,path=folder, 
         file_name= date + '_ssim_channel_by_channel_' + no + '.png', interactive=interactive)

    print(str(datetime.datetime.now())[0:-7] + " : GMSD and SSIM indices and maps computed and saved")

    # MSE & PSNR
    mse_index = mse(input_image,output_image)
    psnr_index = psnr(input_image,output_image)

    # Write in csv file
    with open(folder + date + '_indices.csv', 'a', newline='') as indices:
        indices_writer = csv.writer(indices, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        indices_writer.writerow([no, gmsd_index, ssim_index,mse_index,psnr_index])
    print(str(datetime.datetime.now())[0:-7] + " : Indices saved in csv file")

    # Spectral analysis
    inputOutputSpectra(input_image, output_image, plot_fig=False, save_fig=True,path=folder, 
                       file_name= date + '_power_spectra_' + no + '.png', interactive=interactive)

    differenceSpectrum(input_image, output_image, plot_fig=False, save_fig=True,path=folder, 
                       file_name= date + '_difference_power_spectrum_' + no + '.png', interactive=interactive)

    # Histograms
    histograms(input_image, output_image,plot_fig=False, save_fig=True,path=folder,
               file_name= date + '_histograms_' + no + '.png')

    print(str(datetime.datetime.now())[0:-7] + " : Power spectra computed and saved")


def computeEvaluationTools(date, folder:str = "", no: int = 0, compute: bool = True):

    '''
    Update input and output and compute GMSD and SSIM maps and indices
    args:
        - date: Date when the script is launched
        - folder: Given as argument of the of the scrip
        - no: figure number
    returns:
        - no: incremented figure number
    '''

    # Update input
    input_file = splash.get_object_attribute('image','file')[0]
    input_image = mpimg.imread(input_file)
    print(str(datetime.datetime.now())[0:-7] + " : Input updated")

    width, height, channels = input_image.shape
    if channels == 4:
        warnings.warn("Input image last channel will not be taken into account")
    elif channels != 3:
        raise RuntimeError("Input image must have 3 or 4 channels")
    input_image = input_image[:,:,:3]

    input_output_lists = []

    if mode == Mode.Regular:
        input_output_lists.append(grab(input_image, width, height))

    if mode == Mode.ImmersiveSpace:

        global lock
        lock = True

        image = splash.get_object_attribute("image","file")
        cameras = splash.get_objects_of_type("camera")
        
        for cam in cameras:
            
            for otherCam in cameras:
                splash.set_object_attribute(otherCam,"hide",True)   
            splash.set_object_attribute(cam,"hide",False)
            
            splash.set_object_attribute(cam,"flashBG",'')
            splash.set_object_attribute("image","file",directory + '/' + markers)
            time.sleep(2)
            
            grabber.findCorners()
            grabber.controlMarkerDetection(folder)
            splash.set_object_attribute(cam,"flashBG",'')
            
            splash.set_object_attribute("image","file",image)
            time.sleep(2)
            
            input_output_lists.append(grab(input_image, width, height))

        lock = False

    print(str(datetime.datetime.now())[0:-7] + " : Output updated")

    for i, (input_image, output_image) in enumerate(input_output_lists):
        
        # Plot input and output
        saveInputOutput(input_image,output_image,folder,date + 'cam' + str(i) + '_',str(no), plot_fig=False, interactive=interactive)
        print(str(datetime.datetime.now())[0:-7] + " : Output saved") 
        
        if compute:
            computemetrics(input_image, output_image, date + 'cam' + str(i), folder, str(no), compute)

    print(str(datetime.datetime.now())[0:-7] + " : Plots for input " + str(no) + " saved")

    # Figure no incremented
    no += 1
    return no


def splash_init() -> None:

    # Parse script's arguments
    global folder
    global compute
    global output
    global interactive
    # For camera grabber
    global undistort
    global use_icc
    global merge_mertens
    global markers
    # For sink grabber
    global sixteenbpc
    # Mode
    global mode
    mode = Mode.Regular

    interactive = False # if set to true, interactive plots are saved as well
    compute = True # if set to True, computes every maps and indices for each output, else only loads input and output
    output = Output.Window # output grabber, can be grabbing output from the display window or from a calibrated camera

    sixteenbpc = False

    use_icc = False
    undistort = False
    merge = False
    camera_path = ""
    camera_parameters = ""
    icc_profile = ""
    manual_exposure = False
    exposure = None
    merge_mertens = False
    markers =  "/markers.png"

    try:
        opts, args = getopt.getopt(sys.argv[1:],"hf:QIc:p:i:e:Mm:SP",["folder=","quick","interactive","camera=","parameters=","icc=","exposure=","merge","markers=","sixteenbpc","multiple-projectors"])     
    except getopt.GetoptError:
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print("Options: \n     -f (--folder) [folderName]: set [folderName] as the folder in which data will be saved \n" + 
                  "     -Q (--quick): dont compute maps and indices \n" +
                  "     -c (--camera) [cameraPath]: use camera grabber as output \n" + 
                  "     -I (--interactive): save interactive plots \n" +
                  "     -M (--merge): if enabled, use merge Mertens algorithm for camera capture \n" +
                  "     -p (--parameters) [cameraParameters]: camera intrinsic parameters \n" + 
                  "     -i (--icc) [iccProfile]: camera icc profile \n" +
                  "     -e (--exposure) <exposure>: camera exposure" +
                  "     -m (--markers) [markersFile]: markers image file" +
                  "     -S (--sixteenbpc): set sink in 16bpc" +
                  "     -P (--multiple-projectors): enable immersive space mode")
            sys.exit()
        elif opt in ("-f", "--folder"):
            if arg[-1] == "/" and len(arg) > 1:
                folder = arg
            elif len(arg) > 0:
                folder = arg + "/"
            else:
                raise RuntimeError("Invalid folder path")
        elif opt in ("-Q", "--quick"):
            compute = False
        elif opt in ("-I","--interactive"):
            interactive = True
        elif opt in ("-c", "--camera"):
            output = Output.Camera
            camera_path = arg
        elif opt in ("-p", "--parameters"):
            undistort = True
            camera_parameters = arg
        elif opt in ("-i","--icc"):
            icc_profile = arg
            use_icc = True
        elif opt in ("-e","--exposure"):
            exposure = int(arg)
            manual_exposure = True
        elif opt in ("-M","--merge"):
            merge_mertens = True
        elif opt in ("-m","--markers"):
            markers = arg
        elif opt in ("-S","--sixteenbpc"):
            sixteenbpc = True
        elif opt in ("-P","--multiple-projectors"):
            mode = Mode.ImmersiveSpace

    if output == Output.Window and mode == Mode.ImmersiveSpace:
        raise RuntimeError("Incompatible mode immersive space with sink grabber")

    # Working directory
    global directory
    directory = os.getcwd()
    path = os.path.join(directory, folder)
    try:
        os.mkdir(path)
        print(str(datetime.datetime.now())[0:-7] + " : New folder successfully created")
    except OSError as error:
        if not error.errno == 17: # 17 is when dir already exists
            raise RuntimeError(error)
    
    if interactive:
        try:
            os.mkdir(path+'/Interactive')
        except OSError as error:
            if not error.errno == 17:
                raise RuntimeError(error)
        
    print(str(datetime.datetime.now())[0:-7] + " : Figure will be saved in " + folder)

    # To save indices
    if compute:
        with open(folder + date + '_indices.csv', mode='w') as indices:
            indices_writer = csv.writer(indices, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            indices_writer.writerow(['Input', 'GMSD', 'SSIM','MSE','PSNR'])


    initializeConfig(camera_path,camera_parameters,icc_profile,manual_exposure,exposure)

    # Set a callback for when the image file is changed 
    global attribute_id
    attribute_id = splash.register_attribute_callback("image", "file", onInputChanged)

    print(str(datetime.datetime.now())[0:-7] + " : Initialisation done")


def splash_loop() -> bool:

    global update
    global no
    if update:
        update = False
        no = computeEvaluationTools(date,folder,no,compute)
            
    return True


def splash_stop() -> None:

    print(str(datetime.datetime.now())[0:-7] + " : Quit")