import numpy as np
import cv2
from imutils.perspective import four_point_transform

import datetime
import time

import sys
sys.path.append('../Tools/')
from camera import Camera

class Grabber(Camera):

    def __init__(self, camera_path: str, iterations:int = 10) -> None:

        # Load arUco dictionnary for markers
        self._dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
        self._parameters = cv2.aruco.DetectorParameters_create()
        # Number of frames to compute the projection surface
        self._iterations: int = iterations
        # Dictionary containing all 
        self._detected_corners = {}
        self._projection: np.array = None

        self._projection_corners: np.array = None

        super().__init__(camera_path)


    @property
    def projection(self):
        """
        Get the projection surface
        :return: np.array
        """
        if self._undistort:
            self._projection = four_point_transform(self.undistorted_frame, self._projection_corners)
        else:
            self._projection = four_point_transform(self.frame, self._projection_corners)
        return self._projection

    
    def findCorners(self):
        
        '''
        Detect the aruco marker on the frames to locate the projection's corners
        '''

        self.resolution = [1920,1080]
        # Wait
        start_time = time.time()
        seconds = 1
        while True:
            current_time = time.time()
            elapsed_time = current_time - start_time
            self.grab()
            if elapsed_time > seconds:
                break

        # Initialize corners dictionnary
        self._detected_corners = {}
        for expected_id in range(1,5):
            self._detected_corners[expected_id] = []

        for i in range(self._iterations):
            # Grab the last frame
            self.grab()
            if self.retrieve():
                if self._undistort:
                    self.undistort()
                    new_frame = self.undistorted_frame
                else:
                    new_frame = self.frame

            # Detect the arUco markers
            (marker_corners, ids, rejected) = cv2.aruco.detectMarkers(new_frame, self._dict, parameters=self._parameters)
            for (marker_corner, marker_id) in zip(marker_corners, ids):
                marker_id = int(marker_id)
                if not(marker_id in self._detected_corners.keys()):
                    pass
                corners = marker_corner.reshape((4, 2))
                # Print the marker id if it is detected for the first time
                if len(self._detected_corners[marker_id]) == 0:
                    print(marker_id)
                self._detected_corners[marker_id].append(corners[marker_id-1])

        for marker in range(1,5):
            if len(self._detected_corners[marker]) == 0:
                raise RuntimeError("Unable to detect all the corners")

        top_left = np.mean(np.array(self._detected_corners[1]),0)
        top_right = np.mean(np.array(self._detected_corners[2]),0)
        bottom_right = np.mean(np.array(self._detected_corners[3]),0)
        bottom_left = np.mean(np.array(self._detected_corners[4]),0)
        self._projection_corners = np.array([top_left,top_right,bottom_right,bottom_left])

        print(str(datetime.datetime.now())[0:-7] + " : Projection surface successfully detected")

        return self._projection_corners

    def controlMarkerDetection(self, path):
        '''
        Save image with corner detection to control the accuracy of the marker detection
        '''
        corners = self._projection_corners
        if self._undistort:
            control_detection = self.undistorted_frame
        else:
            control_detection = self.frame
        for i in range(4):
            cv2.circle(control_detection, (int(corners[i][0]), int(corners[i][1])), 7, (0, 0, 255), 1)
            cv2.line(control_detection, (int(corners[i][0]) + 5, int(corners[i][1]) + 5), (int(corners[i][0]) - 5, int(corners[i][1]) - 5), (0, 0, 255), 1)
            cv2.line(control_detection, (int(corners[i][0]) - 5, int(corners[i][1])+ 5), (int(corners[i][0]) + 5, int(corners[i][1]) - 5), (0, 0, 255), 1)
            cv2.putText(control_detection, str(i+1), (int(corners[i][0]) + 5, int(corners[i][1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255))

        cv2.imwrite(path + "marker_detection.png", control_detection)


    def markersGeneration(self, width: int, height: int, marker_size: int = 0, offset: int = 0):
        '''
        Generate an image with markers for projection detection
        '''
        markers = np.ones((width,height)) * 255
        if marker_size == 0:
            marker_size = min(height,width) // 6
        
        # Create the ArUco marker
        top_left_marker = np.zeros((marker_size, marker_size, 1), dtype="uint8")
        top_right_marker = np.zeros((marker_size, marker_size, 1), dtype="uint8")
        bottom_right_marker = np.zeros((marker_size, marker_size, 1), dtype="uint8")
        bottom_left_marker = np.zeros((marker_size, marker_size, 1), dtype="uint8")
        cv2.aruco.drawMarker(self._dict, 1, marker_size, top_left_marker, 1)
        cv2.aruco.drawMarker(self._dict, 2, marker_size, top_right_marker, 1)
        cv2.aruco.drawMarker(self._dict, 3, marker_size, bottom_right_marker, 1)
        cv2.aruco.drawMarker(self._dict, 4, marker_size, bottom_left_marker, 1)
        # Add ArUco markers in the corners
        markers[offset : marker_size + offset, offset : marker_size + offset] = top_left_marker[:,:,0]
        markers[offset : marker_size + offset,-marker_size - offset - 1: - offset -1] = top_right_marker[:,:,0]
        markers[-marker_size - offset - 1: - offset - 1,-marker_size - offset - 1:-1] = bottom_right_marker[:,:,0]
        markers[-marker_size - offset - 1 : - offset - 1,offset : marker_size + offset] = bottom_left_marker[:,:,0]
        
        mpimg.imsave("markers.png",markers)
