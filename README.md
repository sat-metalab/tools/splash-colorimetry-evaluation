[TOC]

# Splash Colorimetry Evaluation

Prototype for [Splash](https://sat-metalab.gitlab.io/splash/en/)'s colorimetric chain evaluation tool. 

To clone this repository run in your terminal:
```bash
git clone https://gitlab.com/sat-metalab/tools/splash-colorimetry-evaluation.git
cd splash-colorimetry-evaluation/
```

## Hardware requirements

For evaluation of Splash's internal colorimetric chain, there is no specific hardware requirement as the output is grabbed directly from a sink.

For projection evaluation you will need:
- at least one projector, more if you wish to evaluate the quality of the blending and of the overlapping areas
- an OpenCV compatible camera
- a checkerboard for color calibration (optional)

At SAT Metalab you can use the following:
- 2 Optoma projectors
- Intel Realsense D435i
- Spyder CHECKR 

## Install dependencies

If you don't have Splash installed yet: here is how to [install Splash](http://sat-metalab.gitlab.io/splash/en/install/contents.html).

Then, to install all the required python libraries run: 
```bash
pip install -r requirements.txt
```
> **Note:** Splash doesn't take into account virtual environment for python scripting for now.  

## Run the Colorimetry Evaluation tool

The [`colo.py`](Script/colo.py) script loads input and output images from Splash and computes several indicators to assess the quality of the colorimetric chain.  

### How to run the script

To run the evaluation tool, run [Splash](https://sat-metalab.gitlab.io/splash/en/) with the [`colo.py`](Script/colo.py) python script:

```bash
cd Script/
splash -P colo.py
```

To evaluate the colorimetric chain on a specific image, you need to load a new image by [changing the media path](http://sat-metalab.gitlab.io/splash/en/explanations/ui/contents.html) in Splash. All the indicators will be computed and saved automatically. By default figures will be saved in the working directory.  
Before loading a new image, you should wait for the previous indicators to be computed which may take a few seconds. Once every plot is computed and saved a `Plots for input <no> saved` message will be printed in the terminal so you know you can load a new input.

There are two main ways of using this tool: by grabbing the output from a sink for evaluation of Splash's internal colorimetric chain or by using a camera to include an evaluation of the projector's quality as well.

#### Evaluation of Splash's internal colorimetric chain

By default, the output is grabbed from a sink linked to the warp window in Splash. This allows to assess the quality of the internal colorimetric chain of Splash without the influence of the projector's quality.  

Make sure that the input is in sRGB to enable an accurate quality assessment. Given that the output of the sink is in linear RGB, it will be converted to sRGB in the `colo.py` script. Note that all of Splash's internal color chain is in linear RGB - only the final rendering of the projection window is in sRGB.

It is possible to specify where you want to save figures to by running the following command and changing `[folder]` to the relative folder path:
```bash
splash -P colo.py -- -f [folder]
```

Use the `--quick` option to load inputs and outputs without further computation. This option allows for a faster computation for each input and will let you compute the indicators afterwards. 
Use the `--interactive` option to save interactive plots. The zoom feature will be enabled and you will be able to edit the plots with a python script.

#### Evaluation of the projection accuracy

To evaluate the quality of the final projection, you can use a camera to grab the output.

You need an OpenCV compatible camera. It is highly recommended to calibrate the camera beforehand. For camera calibration you can use the [calibration tools](/Tools) in the repository. For camera undistortion parameters you should use the same format as described for the [calibration tools](/Tools/README.md). For color correction, if you have a color checkerboard you can specify the camera ICC profile. If not, you can use Merge Mertens algorithm for capturing better quality photos, but it will not allow you to do the white balance.

Then run:
```bash
splash -P colo.py config.json -- -f [folder] -c [cameraPath] -p [cameraParameters] -i [cameraIccProfile]
```

If you did a color calibration with manual exposure, you should specify the same exposure with the option `--exposure`.  
**Beware:** OpenCV handles exposure in different ways depending on the camera model, so if you didn't use OpenCV for frame capture while calibrating there may be no way to find the corresponding exposure value for OpenCV. 

When using a camera you should also specify an adapted configuration file for the projection surface and for the video projector's position. You will need to prepare the configuration before launching the script.

The projection surface is the first thing to be computed when launching the script. It is computed automatically and is never computed again. So once the evaluation script is running you should not move either the projector or the camera otherwise the grabber will not be able to correctly detect the projection area anymore.  
In order to detect the projection surface, the first image to be projected is an image with markers at the corners. Make sure the image is not flipped/flopped in your configuration file **before** launching the tool by loading the marker file as input. Otherwise markers detection will not be possible.  
Once and if the markers have all been detected a message will be printed in the terminal. Make sure the markers were correctly detected so that the projection surface is accurately detected by checking the `marker_detection.png` image saved in the folder passed as argument of the script (or working directory if none was specified).  
You then will be able to load the image you want to perform the evaluation on.  
If the markers were not all detected an error will be thrown and you will have to relaunch the tool. Same if they were detected inaccurately.

![markers](Tools/data/marker_detection.png)  

The corners must be detected in the same order as in the image above. The numbers printed are the corresponding ArUco markers' indices.

For the script to be able to successfully detect the markers, they need to be big enough and to be contrasted enough from the background (the script is not able to detect markers on a black background).  
If the markers are not successfully detected you can try some of the following things:
- Make sure the `Flash Background` is enabled in the main panel of splash when launching the tool
- Add a little offset when generating the markers, but keep in mind that the captured image will be cropped a bit
- If none of this works maybe the markers are too small so try with bigger markers

To generate the markers The image's dimensions need to be adapted to the projection surface size so that there is the less distortion possible. To do so you need to run the `Tools/generate_markers.py` script:
```bash
cd Tools
python3 generate_markers.py --size 600 800 --markers_size 100 --offset 0
```
The offset is by default set to 0.

Then you can specify the markers image file to the script by giving its relative path as argument:
```bash
splash -P colo.py multiproj_config.json -- --folder [folder] --camera [cameraPath] --markers [markersFile]
```

It is also possible to evaluate the quality of the projection in the blending areas with multiple projectors. For this feature you need to be able to capture the whole projection surface with one camera, so it is only possible in simple setups. The simplest way to do so is to use a planar projection surface with two projectors side by side. Technically, there is no limit for the number of projectors or for the projection area disposition.  

- The first thing to do is to generate a configuration file adapted for the setup. For multi-projector configuration follow the [tutorial](https://sat-metalab.gitlab.io/splash/en/tutorials/multi_projector_example.html).
- Then you need to generate an image with markers adapted to the projection area (ie  with the right dimensions). 
- At last run the evaluation tool specifying the right configuration file and markers image file.
 
To evaluate projection in a more complex setup and be able to evaluate the impact of geometric calibration and color calibration, it is possible to add the `--multiple-projectors` argument:  
```bash
splash -P colo.py multiproj_config.json -- --folder [folder] --camera [cameraPath] --markers [markersFile] --multiple-projectors
```

For that specific use case, projectors will be evaluated one by one for each input. Every time you load a new input, markers and then input image will be displayed on each projectors. The mapping must be done such as the markers are undistorted for each projectors, otherwise projection area will not be successfully found.  

**Add photos from immersive space tests in metalab**

#### Arguments summary

All the options are listed below:

| Command | Full Name         | Arguments                  | Description                                        |
|:-------:|:-----------------:|:--------------------------:|----------------------------------------------------|
| `-h`    | `--help`          | -                          | print help                                         |
| `-f`    | `--folder`        | `[folder]`                 | specify the folder where to save all the plots, `folder` must be the relative path to the folder |
| `-c`    | `--camera`        | `[cameraPath]`             | use OpenCV compatible camera grabber for output and specify the camera path |
| `-p`    | `--parameters`    | `[cameraParameters]`       | load camera parameters for undistortion when using camera grabber, `cameraParameters` must be the relative path to the json file where parameters are saved (see the [Calibration](/Tools/README.md) section for the right data format) |
| `-i`    | `--icc`           | `[cameraIccProfile]`       | specify the icc profile file for accurate color correction |
| `-e`    | `--exposure`      | `<exposure>`               | set exposition duration when using a camera        |
| `-I`    | `--interactive`   | -                          | save interactive plots as well as static png plots |
| `-Q`    | `--quick`         | -                          | save input and output and don't compute the evaluation tool in the script |
| `-M`    | `--merge`         | -                          | use Merge Mertens algorithm for camera capture     |
| `-m`    | `--markers`       | `[markersFile]`            | relative path to the generated image with AruCo markers at the corners |
| `-S`    | `--sixteenbpc`    | -                          | set sink rendering in 16 bpc (not supported in last Splash's release) |
| `-P`    | `--multiple-projectors` | -                    | to evaluate the same image on each projector independently |

### Output

All the generated files are named ```yyyy-mm-dd hh:mm:ss_[figname]_<no>.png```, the date being when you launched Splash with the script.

The figures generated are:
- `input_output`: simply a plot with the images
- `gmsd`: GMSD map in gray level
- `gmsd_channel_by_channel`: GMSD maps for each channel in gray level
- `ssim`: SSIM map in gray level
- `ssim_channel_by_channel`: SSIM maps for each channel in gray level
- `power_spectra`: power spectra of both input and output images
- `difference_power_sepctrum`: the difference between the two images and its power spectrum

When using the command: 
```bash
splash -P colo.py -- -i
```
all the figures will also be saved as binary files for interactive plotting, they will be stored in the ```Interactive/``` subfolder under the same names as the png files (only the extension will be changed).  
To load the interactive plots you need to run this python scrip:
```python
import pickle
fig = pickle.load(open('figure_name.fig.pickle', 'rb'))

fig.show() # Show the figure, edit it, etc.!
```

If you wish to run further tests that are not implemented here, the output is also saved in png format.

All those figures will be generated automatically when loading a new input.

```indices.csv``` is a file where GMSD, SSIM, MSE and PSNR indices are stored for each input.

## Jupyter template

The jupyter notebook template provides a convenient way to display results aside with a few explanations on how does each indicator works and should be interpreted. Feel free to adapt it to your own needs. 

To use the [jupyter template](Template/), run in a terminal:
```bash
cd Template/
jupyter-notebook
```

Then run the cells, make sure to set the right paths as arguments of the `loadImages` function.
