# https://gitlab.com/sat-metalab/livepose/-/blob/main/livepose/camera.py

import cv2 # For now only opencv grabber
import numpy as np

import glob
import json
from typing import Any, Dict, Optional, Tuple, Type
import time
from PIL import ImageCms, Image

class Camera:

    '''
    For now only with open cv
    '''
    
    def __init__(self,path: str = ''):

        self._path: str = path

        self._frame: Optional[np.array] = None
        self._frames: Optional[Dict[str, Any]] = {}
        self._undistorted_frame: Optional[np.array] = None

        self._dist_coeffs: Optional[np.array] = None
        self._kmat: Optional[np.array] = None
        self._response: Optional[np.array] = None

        self._camera: cv2.VideoCapture = cv2.VideoCapture()

        self._undistort: bool = False
        self._defaultExposure: Optional[float] = None

        self._icc_profile: Optional[ImageCms.ImageCmsProfile] = None
        self._transform: Optional[ImageCms.ImageCmsTransform] = None


    def is_open(self) -> bool:
        '''
        Check if a video capturing device has been initialized already
        :return: bool
        '''
        return self._camera.isOpened()


    def grab(self) -> bool:
        '''
        Grabs the next frame from video file or capturing device
        :return: bool - in the case of success
        '''
        return self._camera.grab()


    def retrieve(self) -> bool:
        '''
        Decodes and set the grabbed video frame
        grab() should be called beforehand
        :return: bool - true in the case of success
        '''
        ret, self._frame = self._camera.retrieve()
        self._frame = cv2.cvtColor(self._frame, cv2.COLOR_BGR2RGB)
        self._frames["frame"] = self._frame # type: ignore
        
        if not ret:
            self._frame = None
            return False

        return True

    def open(self) -> bool:
        '''
        Open video file or device
        :return: bool - true if successful
        '''
        if self._path == '':
            for camera in glob.glob("/dev/video?"):
                self._camera = cv2.VideoCapture(camera)
                if self.is_open():
                    return self._camera
        else:
            self._camera = cv2.VideoCapture(self._path)
            return self._camera

    def close(self) -> None:
        '''
        Closes video file or capturing device
        '''
        self._camera.release()

    def loadUndistortionParameters(self,camera_parameters):
        '''
        Load distortion coefficients and camera matrix from json file
        '''
        try:
            with open(camera_parameters) as f:
                camera_parameters = json.load(f)
        except:
            return False
        self._dist_coeffs = np.array(camera_parameters['distortion_coeffs'])
        self._kmat = np.reshape(np.array(camera_parameters['camera_matrix']),(3,3))
        self._undistort =  True
        print("Projection error: " + str(camera_parameters['proj_error']))
        return True

    def loadCameraResponseFunction(self, camera_reponse_function):
        '''
        Load inverse camera response fonction from json file
        '''
        try:
            with open(camera_reponse_function) as f:
                response = json.load(f)
        except:
            print("Could not open json file")
            return False
        try:
            self.response = np.reshape(np.array(response["response"]),(256,3))
        except:
            print("Wrong format")
            return False
        print("Inverse camera response function successfully loaded")
        return True

    def loadIccProfile(self, icc_profile):
        '''
        Load camera icc profile and compute transform
        '''
        self._icc_profile = ImageCms.getOpenProfile(icc_profile)
        sRGBProfile = ImageCms.createProfile("sRGB")
        self._transform = ImageCms.buildTransform(self._icc_profile,sRGBProfile,"RGB","RGB")

    def applyTransform(self,image):
        '''
        Apply transform for color calibration
        '''
        im = Image.fromarray(image)
        return np.array(ImageCms.applyTransform(im, self._transform))

    def wait(self,seconds: float = 1):
        '''
        To be sure exposure and frame are 
        '''
        start_time = time.time()
        while True:
            current_time = time.time()
            elapsed_time = current_time - start_time
            self.grab()    
            if elapsed_time > seconds:
                break
    
    def updateCRF(self):
        self.findCorrectExposure()
        self.captureHDR(9,0.33)

    def undistort(self):
        if not self._undistort:
            return False
        self._undistorted_frame = cv2.undistort(self._frame, self._kmat, self._dist_coeffs, None)
        return True
    
    def findCorrectExposure(self):
        
        '''
        Find the exposure which gives correctly exposed photos
        returns:
            - exposure: float = Found exposure duration
        '''
        
        self.wait()
        self.grab()
        self.retrieve()

        width, height = self.resolution
        roiSize = width // 5
        total = roiSize**2

        while True:
            
            exposure = self.exposure
            print("exposure: " + str(exposure))
            s = 0
            
            self.wait()
            self.grab()
            self.retrieve()
            img = self.frame
            
            for y in range(height//2 - roiSize//2, height//2 + roiSize//2):
                for x in range(width//2 - roiSize//2, width//2 + roiSize//2):
                    s += 0.0722*img[y,x,0] + 0.7152*img[y,x,1] + 0.2126*img[y,x,2]
            
            meanValue = s / total
            print("mean value: ",meanValue)
            
            if meanValue >= 100 and meanValue <= 160:
                break
            elif meanValue  < 100.0:
                speed = exposure * max(1.5, 100.0/meanValue)
            else:
                speed = exposure / max(1.5, meanValue/160.0)
            
            self.exposure = speed
            
        self._defaultExposure = exposure
        return True

    def mergeMertens(self,  nbrLDR: int = 3, step: float = 1.0):

        '''
        args:
            - nbrLDR: int = Low Dynamic Range count to use to create the HDR
            - step: float = Stops between successive LDR image
        return:
            - fusion: ldr images merged in one image
        '''

        defaultSpeed = self._defaultExposure
        nextSpeed = defaultSpeed
        
        for steps in range (nbrLDR // 2):
            nextSpeed /= 2.0**step
        
        ldr = []
        expositionDuration = []
        for i in range (nbrLDR):
            self.exposure = nextSpeed
            print(self.exposure)
            self.wait()
            self.grab()
            if self.retrieve():
                ldr.append(self.frame)
                expositionDuration.append(self.exposure)
            
            nextSpeed *= 2.0**step
                
        self.exposure = self._defaultExposure

        isValid = True
        for image in ldr:
            isValid = (np.sum(image) != 0)
                    
        if not isValid:
            raise RuntimeError("invalid frame")

        merge_mertens = cv2.createMergeMertens()
        fusion = merge_mertens.process(ldr)

        return fusion # Test

    def captureHDR(self, nbrLDR: int = 3, step: float = 1.0, computeResponse: bool = False):

        '''
        Capture an HDR image from the camera
        args:
            - nbrLDR: int = Low Dynamic Range count to use to create the HDR
            - step: float = Stops between successive LDR image
            - computeResponse: bool = if true, comptute the camera response function
        '''

        defaultSpeed = self._defaultExposure
        nextSpeed = defaultSpeed
        
        for steps in range (nbrLDR // 2):
            nextSpeed /= 2.0**step
        
        ldr = []
        expositionDuration = []
        for i in range (nbrLDR):
            self.exposure = nextSpeed
            print(self.exposure)
            self.wait()
            self.grab()
            if self.retrieve():
                ldr.append(self.frame)
                expositionDuration.append(self.exposure)
            
            nextSpeed *= 2.0**step
                
        self.exposure = self._defaultExposure
        
        isValid = True
        for image in ldr:
            isValid = (np.sum(image) != 0)
                    
        if not isValid:
            raise RuntimeError("invalid frame")
            
        # expositionDuration needs to be in seconds
        expositionDuration = np.array(expositionDuration)
        expositionDuration = expositionDuration.astype(np.float32)
        
        if computeResponse:
            calibrate = cv2.createCalibrateDebevec()
            self._response = calibrate.process(ldr,expositionDuration)
 
        merge_debevec = cv2.createMergeDebevec()
        hdr = merge_debevec.process(ldr, expositionDuration, self._response)
        
        return hdr, expositionDuration

    @property
    def defaultExposure(self):
        '''
        Get the default exposure
        :return: float - Exposure 
        '''
        return self._defaultExposure

    @defaultExposure.setter
    def defaultExposure(self,exposure):
        '''
        Set the default exposure
        :param: float - Exposure 
        '''
        self._defaultExposure = exposure

    @property
    def exposure(self) -> float:
        '''
        Get the exposure for the camera
        :return: float - Exposure 
        '''
        return float(self._camera.get(cv2.CAP_PROP_EXPOSURE))

    @exposure.setter
    def exposure(self, exposure) -> None:
        '''
        Set the exposure for the camera
        :param: float - Exposure 
        '''
        # If not set to auto THEN to manual doesn't actually change the exposure
        self._camera.set(cv2.CAP_PROP_AUTO_EXPOSURE, 3) # auto mode
        self._camera.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1) # manual mode
        self._camera.set(cv2.CAP_PROP_EXPOSURE, exposure)

    @property
    def resolution(self) -> Tuple[int, int]:
        '''
        Get the resolution of the device
        :return: Tuple[int, int]
        '''
        width = int(self._camera.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(self._camera.get(cv2.CAP_PROP_FRAME_HEIGHT))
        return (width, height)

    @resolution.setter
    def resolution(self, res: Tuple[int, int]) -> None:
        '''
        Set the resolution of the device
        :param: Tuple[int, int]
        '''
        self._camera.set(cv2.CAP_PROP_FRAME_WIDTH, res[0])
        self._camera.set(cv2.CAP_PROP_FRAME_HEIGHT, res[1])

    @property
    def frame(self) -> Optional[np.array]:
        '''
        Get the current frame held by the camera
        :return: np.array - Current decoded frame
        '''
        return self._frame

    @property
    def undistorted_frame(self) -> Optional[np.array]:
        '''
        Get the current undistorted frame held by the camera
        :return: np.array - Current decoded frame
        '''
        self.undistort()
        return self._undistorted_frame

    @property
    def response(self):
        '''
        Get the camera response function
        :return: np.array - previously computed camera response function
        '''
        return self._response

    @response.setter
    def response(self,response: np.array):
        '''
        Set the camera response function
        :param: np.array
        '''
        self._response = response

    @property
    def dist_coeffs(self) -> Optional[np.array]:
        '''
        Get the distortion parameters of the device
        :return: np.array - K matrix
        '''
        return self._dist_coeffs

    @dist_coeffs.setter
    def dist_coeffs(self, dist_coeffs: np.array) -> None:
        '''
        Set the distortion parameters of the device
        :param: np.array - distortion parameters
        '''
        assert(dist_coeffs is None or dist_coeffs.shape == (4, 1)
               or dist_coeffs.shape == (5, 1))
        self._dist_coeffs = dist_coeffs

    @property
    def kmat(self):
        '''
        Get the intrinsics parameters of the device
        :return: np.array - K matrix
        '''
        return self._kmat

    @kmat.setter
    def kmat(self, matrix):
        '''
        Set the K matrix of the device. It is the camera matrix
        representing the intrinsic parameters of the camera.
        It has 9 elements
        :param: np.array - K matrix
        '''
        self._kmat = matrix