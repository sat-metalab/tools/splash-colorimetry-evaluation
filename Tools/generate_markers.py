import argparse
import numpy as np
import cv2
import matplotlib.image as mpimg

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.MetavarTypeHelpFormatter)
    parser.add_argument("-s", "--size", type=int, nargs=2, default=[600, 600], help="Dimension of the generated image. For image with width of 800 and height of 600, input values like this: 800 600")
    parser.add_argument("-m", "--markers_size", type=int, default=0, help="Size of the markers")
    parser.add_argument("-o", "--offset", type=int, default=0, help="Offset")
    return parser.parse_args()

def markersGeneration(width: int, height: int, marker_size: int = 0, offset: int = 0):
    '''
    Generate an image with markers for projection detection
    '''
    dictionnary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
    markers = np.ones((width,height)) * 255
    if marker_size == 0:
        marker_size = min(height,width) // 6
    
    # Create the ArUco marker
    top_left_marker = np.zeros((marker_size, marker_size, 1), dtype="uint8")
    top_right_marker = np.zeros((marker_size, marker_size, 1), dtype="uint8")
    bottom_right_marker = np.zeros((marker_size, marker_size, 1), dtype="uint8")
    bottom_left_marker = np.zeros((marker_size, marker_size, 1), dtype="uint8")
    cv2.aruco.drawMarker(dictionnary, 1, marker_size, top_left_marker, 1)
    cv2.aruco.drawMarker(dictionnary, 2, marker_size, top_right_marker, 1)
    cv2.aruco.drawMarker(dictionnary, 3, marker_size, bottom_right_marker, 1)
    cv2.aruco.drawMarker(dictionnary, 4, marker_size, bottom_left_marker, 1)
    # Add ArUco markers in the corners
    markers[offset : marker_size + offset, offset : marker_size + offset] = top_left_marker[:,:,0]
    markers[offset : marker_size + offset, height - marker_size - offset : height - offset] = top_right_marker[:,:,0]
    markers[ width - marker_size - offset : width - offset , height - marker_size - offset : height - offset] = bottom_right_marker[:,:,0]
    markers[ width - marker_size - offset : width - offset , offset : marker_size + offset] = bottom_left_marker[:,:,0]
    
    mpimg.imsave("markers.png",markers,cmap='gray')

if __name__ == "__main__":
    args = parse_args()
    markersGeneration(args.size[0],args.size[1],args.markers_size,args.offset)
    print("Image generated and saved in working directory")