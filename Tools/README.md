[TOC]

# Camera Calibration

## Calibration of the intrinsic parameters

[Original code](https://gitlab.com/sat-metalab/livepose/-/tree/main/tools/addon_splash) for intrinsic parameters calibration.

`chessboard_calibrate.py` is a tool to estimate the parameters of the camera using a checkerboard pattern. It will save the camera parameters to a file. You can find a checkerboard pattern [here](data/chessboard_pattern.png). Take care to print or copy the checkerboard pattern on a perfectly flat surface (a rigid cardboard for example).

Run in the terminal:
```bash
python3 chessboard_calibrate.py --camera /path/to/camera --square_size 21.0 --pattern_size 10 7
```
While default values are provided, two options are required to have proper results: 
* The option `square_size` corresponds to the size of an individual square of the checkerboard pattern in millimeters. 
* The other parameter is the `pattern_size`, it is the dimensions of the pattern. It accepts two integers corresponding to the number of corners of the chessboard. The first one is the number of columns of the checkerboard pattern, the second integer is the number of rows of the pattern.

To see the available options and their description:
```bash
python3 chessboard_calibrate.py --help
```

![Intrinsics calibration](data/intrinsics_calibration.png)

Once launched a window will appear, showing the live stream from the camera. The framerate will be low until it detects the pattern. It will get faster once the pattern is detected. Move the camera around while keeping the pattern in the frame until the calibration is completed. If calibration is successful it will create a file name `camera_parameters.json` in the current directory which will look like this:

```json
{
    "proj_error": 0.8124848502163823,
    "camera_matrix": [
        630.1721161286164,
        0.0,
        340.1573821117727,
        0.0,
        634.67835231384,
        254.52840135912254,
        0.0,
        0.0,
        1.0
    ],
    "distortion_coeffs": [
        0.06343236777295372,
        0.4240464565567057,
        0.0,
        0.0,
        -1.6977034092945615
    ],
    "frame_resolution": [
        640,
        480
    ]
}
```

The various sections are:

* `proj_error`: computed reprojection error, in pixels. The lower the better, here it is a bit less than one pixel.
* `camera_matrix`: camera matrix holding the horizontal and vertical fields of view, as well as the optical center position.
* `distortion_coeffs`: distortion coefficients for the camera.
* `frame_resolution`: resolution of the captured frame, as this impacts the computed values.

## Color Calibration

If you possess a Color Checker Board, the best solution is to calibrate the camera is to compute the icc profile. 
 
As with any color profiling task, it is important to setup a known and repeatable image processing flow, to ensure that the resulting profile will be usable.

You can use tools such as [Argyll](https://argyllcms.com/doc/Scenarios.html#PS1) for profiling your camera.

For Argyll installation run the following command:
```bash
sudo apt install argyll
```

Firstly the chart should be captured and saved to a TIFF format file. The raster file need only be roughly cropped so as to contain the test chart (including the charts edges).  

The following script should allow you to capture a frame in tiff format with the `Camera` class defined in the `camera.py` script:

```python
from camera import Camera
from PIL import image

# Open device
cam = Camera('/dev/video0')
cam.open()

# Find correct exposure and grab frame
cam.findCorrectExposure()
cam.wait() # to be sure the correct exposure is applied

# Grab and save file
cam.grab()
if cam.retrieve():
    frame = Image.fromarray(cam.frame)
    frame.save('camera.tif')
```

![spyderChecker](data/camera.png)

Here the light conditions are meant to be the closest to the environment we will be working with: dark environment with white projection. The image has been cropped.

The second step is to extract the RGB values from the `camera.tif` file, and match them to the reference CIE values.

For that purpose you will need a template .cht file that describes the features of the chart, and how the test patches are labeled. Also needed is a file containing the CIE values for each of the patches in the chart, which is typically supplied with the chart, available from the manufacturers web site, or has been measured using a spectrometer. For some color checker board the files are also available on the [Argyll repo](https://github.com/beku/Argyll-Releases/tree/master/ref).

Then you simply have to run the following command:
```bash
scanin -v camera.tif color_checker.cht color_checker.cie
colprof -v -D"Camera" -qm -am -u camera
```

The file will be saved under the name `camera.icc`.
